///TODO List
/// - Collision Detection   x
/// - Bouncing Particles    x
/// - Sliding Particles     x
/// - Demo with permanent particles.
/// - Keybindings for dynamic stuff
///     - Position
///     - Direction
///     - Randomness


#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <vector>
//#include "glm/glm.hpp"

#include <GL/glew.h>   //Include order can matter here
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtx/string_cast.hpp"

enum EmitterType {
    NONE,
    SQUARE,
    CUBE,
    DISK,
    SPHERE
};

class Intersection {
    public:
        glm::vec3 pos;
        glm::vec3 norm;
        float dsq;
};

class Particle {
    public:
        Particle();
        Particle(const glm::vec3& position, const glm::vec3& velocity, 
                 const glm::vec3& color);
        Particle(const glm::vec3& position, const glm::vec3& velocity, const glm::vec3& color,
                 float life, float liferand, const glm::vec3& grav);
        Particle(const glm::vec3& position, const glm::vec3& velocity, const glm::vec3& color,
                 float life, float liferand, float colrand, float dirrand, const glm::vec3& grav, 
                 float bouncy, float termvel);
        Particle(const glm::vec3& position, const glm::vec3& velocity, const glm::vec3& color,
                 const glm::vec3& c2, float life, float liferand, float colrand, 
                 float dirrand, const glm::vec3& grav, float bouncy, float termvel);
        
        bool update(float* vertices, int size, const std::vector<glm::mat4>& model, int stride, bool grav, const glm::vec3& force = glm::vec3(0.0f)); //< returns false if the particle is to be destroyed
        bool update(bool grav, const glm::vec3& force = glm::vec3(0.0f));
        
        void setLifetime(float life) { lifetime = life; }
        void setGravity(float grav) { gravity = glm::vec3(0, grav, 0); }
        void setPosition(const glm::vec3& position) { pos = position; }
        void setVelocity(const glm::vec3& velocity) { vel = velocity; }
        void setColor(const glm::vec3& color) { col1 = col = color; }
        void setColor2(const glm::vec3& color) { col2 = color; }
        void markDead() { curLife = lifetime; }
        void setBounciness(float b) { bounce = b; }
        void resetLife() { curLife = 0; }
        void colorChange() { changeColor = !changeColor; }
        
        float getLifetime() const { return lifetime; }
        float getCurrentLife() const { return curLife; }
        float getLifeRatio() const { return curLife / lifetime; }
        glm::vec3 getGravity() const { return gravity; }
        glm::vec3 getPosition() const { return pos; }
        glm::vec3 getVelocity() const { return vel; }
        glm::vec3 getColor() const { return col; }
    private:
        Intersection* intersect(const glm::vec3& pos, const glm::vec3& dir, glm::vec3 verts[]);
    
        float termVel;
        float curLife;
        float lifetime;
        glm::vec3 gravity;
        glm::vec3 pos;
        glm::vec3 vel;
        glm::vec3 col;
        glm::vec3 col1;
        glm::vec3 col2;
        float bounce;
        bool changeColor;
};

class Emitter {
    public:
        Emitter();
        Emitter(bool start);
        Emitter(int numPerSec);
        Emitter(int numPerSec, bool start);
        
        int render();
        void update(float* vertices, int size, const std::vector<glm::mat4>& model, int stride, const glm::vec3& force = glm::vec3());
        void pause();
        void resume();
        void stop();
        void start();
        bool isPaused() { return paused; }
        bool isRunning() { return run; }
        
        void setParticleRate(float numPerSec) { numParticles = numPerSec; }
        void setDirection(const glm::vec3& direction) { dir = glm::normalize(direction); }
        void setPosition(const glm::vec3& position) { pos = position; }
        void setColor(const glm::vec3& color) { col = color; }
        void setSecondColor(const glm::vec3& c) { col2 = c; }
        void setVelocity(float velocity) { vel = velocity; }
        void setParticleLife(float life) { plife = life; }
        void setLifeRand(float rand) { liferand = rand; }
        void setColorRand(float rand) { colrand = rand; }
        void setDirectionRand(float rand) { dirrand = rand; }
        void setShaderProgram(int prog) { shaderProgram = prog; }
        void toggleGravity() { usegrav = !usegrav; }
        void setEmitterData(const glm::vec2& d) { eData = d; }
        void setBounciness(float bounce) { bratio = clamp(bounce, 0.0f, 1.0f); }
        void setTermVelocity(float vel) { tvel = vel; }
        void toggleTexture() { useTex = !useTex; }
        void setGravity(const glm::vec3& gravity) { grav = gravity; }
        void makeFire();
        void setTimer(float time) { endTime = time; timer = 0; }
        void resetTimer() { timer = 0; }
        void pulse();
        void setEmitterType(enum EmitterType t) { type = t; }
        void toggleTexBoard() { texBoard = !texBoard; }
        void loadTexture(std::string tex);
        void colorChange() { changeColor = !changeColor; }
        
        float getParticleRate() const { return numParticles; }
        glm::vec3 getPosition() const { return pos; }
        glm::vec3 getDirection() const { return dir; }
        glm::vec3 getColor() const { return col; }
        glm::vec3 getGravity() const { return grav; }
        float getVelocity() const { return vel; }
        float getLifeTime() const { return plife; }
        float getLifeRand() const { return liferand; }
        float getColorRand() const { return colrand; }
        float getDirectionRand() const { return dirrand; }
        bool usingGravity() const { return usegrav; }
        glm::vec2 getEmitterData() const { return eData; }
        float getBounciness() const { return bratio; }
        float getTermVelocity() const { return tvel; }
        bool usingTexture() const { return useTex; }
        bool isTimeUp() { return timer >= endTime; }
        
        std::vector<Particle> getParticles() const { return particles; }
        std::vector<bool> getLiveList() const { return curParticles; }
        void quicksort(const glm::vec3& cpos, int low, int high);
    private:
        std::vector<bool> curParticles;
        std::vector<Particle> particles;
        float plife;
        float numParticles;
        bool run;
        bool paused;
        float lastTime;
        float timer;
        float endTime;
        bool useTex;
        bool fire;
        bool texBoard;
        glm::vec2 texWH;
        bool changeColor;
        
        int shaderProgram;
        
        //emitter data
        enum EmitterType type;
        glm::vec3 pos;
        glm::vec3 dir;
        glm::vec3 col;
        glm::vec3 col2;
        float vel;
        bool usegrav;
        glm::vec2 eData;
        float bratio;
        float tvel;
        GLuint tex0;
        glm::vec3 grav;
        
        //Particle refactor
        int plimit;
        float refTime;
        float time;
        float tottime;
        
        //Randomness
        float liferand;
        float colrand;
        float dirrand;
        
        int partition(const glm::vec3& cpos, int low, int high);
        float distSq(const glm::vec3 cpos, const Particle& p);
        void refactor();
        void printParticlePos();
        float clamp(float value, float min, float max) { return (value < min) ? min : (value > max) ? max : value; }
};

#endif
