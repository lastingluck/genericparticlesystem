#version 140

in vec3 fColor;
in vec3 normal;
in vec3 pos;
in vec3 lightDir;

uniform sampler2D tex;
uniform int texID;
uniform int particle;

uniform int fullSize;
uniform int texSize;
uniform int texNum;
uniform float lifeRatio;

out vec4 outColor;

const float ambient = 0.5f;
void main() {
    float r = dot(gl_PointCoord-0.5, gl_PointCoord-0.5);
    if(particle > 0.5f && r > 0.25) {
        discard;
    }
    else {
        vec3 color;
        float alpha = 1.0f;
        if(texID == -1) {
            color = fColor;
            if(particle > 1.5f) {
                alpha = 1.0f - (2.0 * sqrt(r));
            }
        }
        else {
            vec2 texCoord = vec2((gl_PointCoord.x*texSize+(mod(texNum, 4)*texSize))/fullSize, (gl_PointCoord.y*texSize+(texNum/4)*texSize)/fullSize);
            int tnum;
            if(texNum < 0.5f) {
                tnum = texNum+1;
            }
            else {
                tnum = texNum-1;
            }
            vec2 texCoord2 = vec2((gl_PointCoord.x*texSize+(mod(tnum, 4)*texSize))/fullSize, (gl_PointCoord.y*texSize+(tnum/4)*texSize)/fullSize);
            color = texture(tex, texCoord).rgb;
            vec3 color2 = texture(tex, texCoord2).rgb;
            if(texNum < 0.5f) {
                color = mix(color, color2, lifeRatio);
            }
            else {
                color = mix(color2, color, lifeRatio);
            }
            //if(color.x < 0.05f && color.y < 0.05f && color.z < 0.05f) {
            //    discard;
            //}
            //alpha = 1.0f - (2.0 * sqrt(r));
            alpha = (2-color.x+color.y+color.z) / 3.0f;
        }
        vec3 diffuseC = color*max(dot(-lightDir,normal),0.0);
        vec3 ambC = color*ambient;
        vec3 viewDir = normalize(-pos); //We know the eye is at (0,0)!
        vec3 reflectDir = reflect(viewDir,normal);
        float spec = max(dot(reflectDir,lightDir),0.0);
        if (dot(-lightDir,normal) <= 0.0)spec = 0;
        vec3 specC = vec3(1.0,1.0,1.0)*pow(spec,4);
        outColor = vec4(ambC+diffuseC+specC, alpha);
    }
}
